
import { BrowserRouter } from 'react-router-dom';
import AppRouter from './components/AppRoutes';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import './styles/App.css';

const API_KEY: string = 'a64de7d8108e58331932a4d385576674';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <AppRouter />
        <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
