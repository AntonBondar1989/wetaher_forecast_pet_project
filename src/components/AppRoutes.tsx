import { Route, Routes } from "react-router-dom";
import KyivPage from "./pages/KyivPage";
import LondonPage from "./pages/LondonPage";
import Main from "./pages/Main";
import SydneyPage from "./pages/SydneyPage";
import TokyoPage from "./pages/TokyoPage";

const AppRouter: React.FC = () => {
   return (
      <Routes>
         <Route path="/main" element={<Main />} />
         <Route path="/" element={<Main />} />
         <Route path="/kyiv" element={<KyivPage />} />
         <Route path="/tokyo" element={<TokyoPage />} />
         <Route path="/london" element={<LondonPage />} />
         <Route path="/sydney" element={<SydneyPage />} />
      </Routes>
   )
}

export default AppRouter;