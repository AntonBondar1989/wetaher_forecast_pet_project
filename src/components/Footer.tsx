import '../styles/Footer.css'


const Footer: React.FC = () => {
   return (
      <div className="footer">
        © 2022 All Rights Reserved
      </div>
   )
}

export default Footer;