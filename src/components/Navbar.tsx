import React from "react";
import { Link } from "react-router-dom";

import '../styles/Navbar.css';


const Navbar: React.FC = () => {

   const flagLogo = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/flagUkraine.png');
   
   return (
      <div className="container navbar_container ">
         <div className="row navbar_row">
            <div className="col-sm box_logo">
            <Link to='./'><img className="logo_img" src={flagLogo} alt="logo" /></Link>
            </div>
            <div className="col-sm">
               <ul className="navbar_ul">
                  <Link to='./main'><li>Main</li></Link>
                  <Link to='./kyiv'><li>Kyiv</li></Link>
                  <Link to='./tokyo'><li>Tokyo</li></Link>
                  <Link to='./london'><li>London</li></Link>
                  <Link to='./sydney'><li>Sydney</li></Link>
               </ul>
            </div>
         </div>
      </div>
   )
}

export default Navbar;