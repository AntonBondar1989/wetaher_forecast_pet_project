import { useEffect, useMemo } from "react"
import { useActions } from "../../hooks/useAction"
import { useTypedSelector } from "../../hooks/useTypedSelector"

import "../../styles/WeatherScreenKyiv.css";

const KyivPage: React.FC = () => {

   // деструктуризация (свой Хук)
   const { error, loading, data } = useTypedSelector(state => state.Kyiv)

   // вместо каждый раз диспатча (хук)
   const { fetchKyiv } = useActions()

   // вложеный обьект(страхуемся что бы не получить undefined)
   const temp = data.main && data.main.temp
   const tempMin = data.main && data.main.temp_min
   const tempMax = data.main && data.main.temp_max
   const wind = data.wind && data.wind.speed
   const main = data.weather && data.weather[0] && data.weather[0].main
   const description = data.weather && data.weather[0] && data.weather[0].description

   // PNG SVG JPEG
   const windLogo1 = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/wind.png');
   const loader = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/Loader3.png');
   const errorPng = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/error.png');
   const cloudsPng = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/Clouds.png');
   const clearPng = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/Clear.png');
   const snowPng = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/Snow.png');
   const rainPng = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/Rain.png');
   const extremePng = require('/Anton/Personal Works/weather-forecast-react-redux-ts/src/img/Extreme.png');

   const weather = useMemo(() => {
      switch (main) {
         case 'Clouds':
            return <img className="logo_weather_kyiv" src={cloudsPng} alt="logo" />;
         case 'Clear':
            return <img className="logo_weather_kyiv" src={clearPng} alt="logo" />;
         case 'Snow':
            return <img className="logo_weather_kyiv" src={snowPng} alt="logo" />;
         case 'Mist':
            return <img className="logo_weather_kyiv" src={cloudsPng} alt="logo" />;
         case 'Rain':
            return <img className="logo_weather_kyiv" src={rainPng} alt="logo" />;
         case 'Extreme':
            return <img className="logo_weather_kyiv" src={extremePng} alt="logo" />;
         default:
            return main;
      }
   }, [main]);

   useEffect(() => {
      fetchKyiv()
   }, [])

   if (loading) {
      // можно какую то крутилку добавить
      return <h1 className="loadingScreen"><img src={loader} alt="loader" /></h1>
   }
   if (error) {
      // можно свою ошибку вставить 
      return <h1 className="errorScreen"><img src={errorPng} alt="error" /><br />Something is wrong!</h1>
   }

   return (
      <div className="screen_kyiv">
         <div className="description_kyiv">
            <h1>{data.name}</h1>
            <h1>
               {Math.round(temp)}&#8451;&nbsp;{weather}
            </h1>
            <div>{description}...</div>
            <div>min.: {Math.round(tempMin)}&#8451;&nbsp;&nbsp; max.: {Math.round(tempMax)}&#8451; </div>
            <div><img src={windLogo1} alt="logo" /> wind: {wind} m/sec.</div>
         </div>

      </div>
   )
}

export default KyivPage;