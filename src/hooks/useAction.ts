import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import ActionCreators from '../store/action-creator/';


// суть в том что мы связываем все actioncreator с этим диспатч и больше диспатч нам не понадобится (облегчаем себе работу)
export const useActions = () => {
   const dispatch = useDispatch();
   return bindActionCreators(ActionCreators, dispatch)
}