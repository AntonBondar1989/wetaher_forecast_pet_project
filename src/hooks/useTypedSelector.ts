import { useSelector } from "react-redux";
import { TypedUseSelectorHook } from "react-redux";
import { RootState } from "../store/reducer";


// Свой хук для того что бы выцепить стейт типизированый (отследи еще где rootState)
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;