import { KyivAction, KyivActionTypes } from './../types/Kyiv';
import { Dispatch } from 'redux';
import axios from 'axios';

export const fetchKyiv = () => {
   return async (dispatch: Dispatch<KyivAction>) => {
      try {
         dispatch({ type: KyivActionTypes.FETCH_KYIV })
         const response = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=Kyiv&appid=a64de7d8108e58331932a4d385576674&units=metric')
         setTimeout(() => {
            dispatch({
               type: KyivActionTypes.FETCH_KYIV_SUCCESS,
               payload: response.data
            })
         }, 500)
      } catch (error) {
         dispatch({
            type: KyivActionTypes.FETCH_KYIV_ERROR,
            payload: "Произошла ошибка при загрузке пользователей"
         })
      }
   }
}