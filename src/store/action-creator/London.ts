import { LondonAction, LondonActionTypes } from './../types/London';
import { Dispatch } from 'redux';
import axios from 'axios';


export const fetchLondon = () => {
   return async (dispatch: Dispatch<LondonAction>) => {
      try {
         dispatch({ type: LondonActionTypes.FETCH_LONDON })
         const response = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=London&appid=a64de7d8108e58331932a4d385576674&units=metric')
         setTimeout(() => {
            dispatch({
               type: LondonActionTypes.FETCH_LONDON_SUCCESS,
               payload: response.data
            })
         }, 500)
      } catch (error) {
         dispatch({
            type: LondonActionTypes.FETCH_LONDON_ERROR,
            payload: "Произошла ошибка при загрузке пользователей"
         })
      }
   }
}