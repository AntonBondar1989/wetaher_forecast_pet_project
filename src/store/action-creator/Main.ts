import { MainAction, MainActionTypes } from './../types/Main';
import { Dispatch } from 'redux';
import axios from 'axios';

export const fetchMain = (cityFetch: string) => {
   return async (dispatch: Dispatch<MainAction>) => {
      try {
         dispatch({ type: MainActionTypes.FETCH_MAIN })
         const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${cityFetch}&appid=a64de7d8108e58331932a4d385576674&units=metric`)
         setTimeout(() => {
            dispatch({
               type: MainActionTypes.FETCH_MAIN_SUCCESS,
               payload: response.data
            })
         }, 500)
      } catch (error) {
         dispatch({
            type: MainActionTypes.FETCH_MAIN_ERROR,
            payload: "Произошла ошибка при загрузке пользователей"
         })
      }
   }
}