import { SydneyAction, SydneyActionTypes } from './../types/Sydney';
import { Dispatch } from 'redux';
import axios from 'axios';

export const fetchSydney = () => {
   return async (dispatch: Dispatch<SydneyAction>) => {
      try {
         dispatch({ type: SydneyActionTypes.FETCH_SYDNEY })
         const response = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=Sydney&appid=a64de7d8108e58331932a4d385576674&units=metric')
         setTimeout(() => {
            dispatch({
               type: SydneyActionTypes.FETCH_SYDNEY_SUCCESS,
               payload: response.data
            })
         }, 500)
      } catch (error) {
         dispatch({
            type: SydneyActionTypes.FETCH_SYDNEY_ERROR,
            payload: "Произошла ошибка при загрузке пользователей"
         })
      }
   }
}