import { TokyoAction, TokyoActionTypes } from './../types/Tokyo';
import { Dispatch } from 'redux';
import axios from 'axios';

export const fetchTokyo = () => {
   return async (dispatch: Dispatch<TokyoAction>) => {
      try {
         dispatch({ type: TokyoActionTypes.FETCH_TOKYO })
         const response = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=Tokyo&appid=a64de7d8108e58331932a4d385576674&units=metric')
         setTimeout(() => {
            dispatch({
               type: TokyoActionTypes.FETCH_TOKYO_SUCCESS,
               payload: response.data
            })
         }, 500)
      } catch (error) {
         dispatch({
            type: TokyoActionTypes.FETCH_TOKYO_ERROR,
            payload: "Произошла ошибка при загрузке пользователей"
         })
      }
   }
}