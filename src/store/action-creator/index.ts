import * as LondonActionCreator from './London';
import * as KyivActionCreator from './Kyiv';
import * as TokyoActionCreator from './Tokyo';
import * as SydneyActionCreator from './Sydney';
import * as MainActionCreator from './Main';


// обьеденяем все экшены
export default {
   ...LondonActionCreator,
   ...KyivActionCreator,
   ...TokyoActionCreator,
   ...SydneyActionCreator,
   ...MainActionCreator,
}