import { KyivState, KyivAction, KyivActionTypes } from './../types/Kyiv';

// State
const initialState: KyivState = {
   data: {},
   loading: false,
   error: null
}

export const KyivReducer = (state = initialState, action: KyivAction): KyivState => {
   switch (action.type) {
      case KyivActionTypes.FETCH_KYIV:
         return { loading: true, error: null, data: {} }
      case KyivActionTypes.FETCH_KYIV_SUCCESS:
         return { loading: false, error: null, data: action.payload }
      case KyivActionTypes.FETCH_KYIV_ERROR:
         return { loading: false, error: action.payload, data: {} }
      default:
         return state;
   }
}