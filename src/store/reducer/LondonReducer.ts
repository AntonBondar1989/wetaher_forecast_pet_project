import { LondonState, LondonAction, LondonActionTypes } from './../types/London';

// State
const initialState: LondonState = {
   data: {},
   loading: false,
   error: null
}

export const LondonReducer = (state = initialState, action: LondonAction): LondonState => {
   switch (action.type) {
      case LondonActionTypes.FETCH_LONDON:
         return { loading: true, error: null, data: {} }
      case LondonActionTypes.FETCH_LONDON_SUCCESS:
         return { loading: false, error: null, data: action.payload }
      case LondonActionTypes.FETCH_LONDON_ERROR:
         return { loading: false, error: action.payload, data: {} }
      default:
         return state;
   }
}