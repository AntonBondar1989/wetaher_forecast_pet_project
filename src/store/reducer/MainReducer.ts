import { MainState, MainAction, MainActionTypes } from './../types/Main';

// State
const initialState: MainState = {
   data: {},
   loading: false,
   error: null
}

export const MainReducer = (state = initialState, action: MainAction): MainState => {
   switch (action.type) {
      case MainActionTypes.FETCH_MAIN:
         return { loading: true, error: null, data: {} }
      case MainActionTypes.FETCH_MAIN_SUCCESS:
         return { loading: false, error: null, data: action.payload }
      case MainActionTypes.FETCH_MAIN_ERROR:
         return { loading: false, error: action.payload, data: {} }
      default:
         return state;
   }
}