import { SydneyState, SydneyAction, SydneyActionTypes } from './../types/Sydney';

// State
const initialState: SydneyState = {
   data: {},
   loading: false,
   error: null
}

export const SydneyReducer = (state = initialState, action: SydneyAction): SydneyState => {
   switch (action.type) {
      case SydneyActionTypes.FETCH_SYDNEY:
         return { loading: true, error: null, data: {} }
      case SydneyActionTypes.FETCH_SYDNEY_SUCCESS:
         return { loading: false, error: null, data: action.payload }
      case SydneyActionTypes.FETCH_SYDNEY_ERROR:
         return { loading: false, error: action.payload, data: {} }
      default:
         return state;
   }
}