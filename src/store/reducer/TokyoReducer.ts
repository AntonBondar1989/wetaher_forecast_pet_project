import { TokyoState, TokyoAction, TokyoActionTypes } from './../types/Tokyo';

// State
const initialState: TokyoState = {
   data: {},
   loading: false,
   error: null
}

export const TokyoReducer = (state = initialState, action: TokyoAction): TokyoState => {
   switch (action.type) {
      case TokyoActionTypes.FETCH_TOKYO:
         return { loading: true, error: null, data: {} }
      case TokyoActionTypes.FETCH_TOKYO_SUCCESS:
         return { loading: false, error: null, data: action.payload }
      case TokyoActionTypes.FETCH_TOKYO_ERROR:
         return { loading: false, error: action.payload, data: {} }
      default:
         return state;
   }
}