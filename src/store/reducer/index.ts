import { MainReducer } from './MainReducer';
import { SydneyReducer } from './SydneyReducer';
import { TokyoReducer } from './TokyoReducer';
import { KyivReducer } from './KyivReducer';
import { LondonReducer } from "./LondonReducer";
import {combineReducers} from "redux";

export const rootReducer = combineReducers( {
   London: LondonReducer,
   Kyiv: KyivReducer,
   Tokyo: TokyoReducer,
   Sydney: SydneyReducer,
   Main: MainReducer,
   
})

// Для того что бы выцепить стейт
export type RootState = ReturnType<typeof rootReducer>