


// TS
export interface KyivState {
   data: any;
   loading: boolean;
   error: null | string;
}

export enum KyivActionTypes {
   FETCH_KYIV = 'FETCH_KYIV',
   FETCH_KYIV_SUCCESS = 'FETCH_KYIV_SUCCESS',
   FETCH_KYIV_ERROR = 'FETCH_KYIV_ERROR',
}

interface FetchKyivAction {
   type: KyivActionTypes.FETCH_KYIV;
}

interface FetchKyivSuccessAction {
   type: KyivActionTypes.FETCH_KYIV_SUCCESS;
   payload: any
}

interface FetchKyivErrorAction {
   type: KyivActionTypes.FETCH_KYIV_ERROR;
   payload: string;
}

export type KyivAction = FetchKyivAction | FetchKyivSuccessAction | FetchKyivErrorAction 
