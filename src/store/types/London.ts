


// TS
export interface LondonState {
   data: any;
   loading: boolean;
   error: null | string;
}

export enum LondonActionTypes {
   FETCH_LONDON = 'FETCH_LONDON',
   FETCH_LONDON_SUCCESS = 'FETCH_LONDON_SUCCESS',
   FETCH_LONDON_ERROR = 'FETCH_LONDON_ERROR',
}

interface FetchLondonAction {
   type: LondonActionTypes.FETCH_LONDON;
}

interface FetchLondonSuccessAction {
   type: LondonActionTypes.FETCH_LONDON_SUCCESS;
   payload: any
}

interface FetchLondonErrorAction {
   type: LondonActionTypes.FETCH_LONDON_ERROR;
   payload: string;
}

export type LondonAction = FetchLondonAction | FetchLondonSuccessAction | FetchLondonErrorAction 
