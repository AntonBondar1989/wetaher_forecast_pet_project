


// TS
export interface SydneyState {
   data: any;
   loading: boolean;
   error: null | string;
}

export enum SydneyActionTypes {
   FETCH_SYDNEY = 'FETCH_SYDNEY',
   FETCH_SYDNEY_SUCCESS = 'FETCH_SYDNEY_SUCCESS',
   FETCH_SYDNEY_ERROR = 'FETCH_SYDNEY_ERROR',
}

interface FetchSydneyAction {
   type: SydneyActionTypes.FETCH_SYDNEY;
}

interface FetchSydneySuccessAction {
   type: SydneyActionTypes.FETCH_SYDNEY_SUCCESS;
   payload: any
}

interface FetchSydneyErrorAction {
   type: SydneyActionTypes.FETCH_SYDNEY_ERROR;
   payload: string;
}

export type SydneyAction = FetchSydneyAction | FetchSydneySuccessAction | FetchSydneyErrorAction 
