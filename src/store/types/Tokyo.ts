


// TS
export interface TokyoState {
   data: any;
   loading: boolean;
   error: null | string;
}

export enum TokyoActionTypes {
   FETCH_TOKYO = 'FETCH_TOKYO',
   FETCH_TOKYO_SUCCESS = 'FETCH_TOKYO_SUCCESS',
   FETCH_TOKYO_ERROR = 'FETCH_TOKYO_ERROR',
}

interface FetchTokyoAction {
   type: TokyoActionTypes.FETCH_TOKYO;
}

interface FetchTokyoSuccessAction {
   type: TokyoActionTypes.FETCH_TOKYO_SUCCESS;
   payload: any
}

interface FetchTokyoErrorAction {
   type: TokyoActionTypes.FETCH_TOKYO_ERROR;
   payload: string;
}

export type TokyoAction = FetchTokyoAction | FetchTokyoSuccessAction | FetchTokyoErrorAction 
